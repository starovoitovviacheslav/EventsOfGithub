//
//  NetworkManager.swift
//  githubEvents
//
//  Created by Slava Starovoitov on 25.08.2021.
//

import Foundation

class NetworkManager {
    
    public enum Result<T> {
        case success(T)
        case failure(Error)
    }
    
    static var shared = NetworkManager()
    
    public func getData(pageNum: Int, comletionHandler: @escaping ([GitResult]) -> Void) {

        let apiURL = "https://api.github.com/events?page=\(String(pageNum))"
        
        guard let url = URL(string: apiURL) else {
            print("cant convert string to url")
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard (response as? HTTPURLResponse) != nil else {
                print("no response")
                return
            }
            
            guard error == nil else {
                print("catch error")
                return
            }
            
            guard let data = data else {
                print("no data")
                return
            }
            
            print(data)
            
            do {
                let json = try? JSONDecoder().decode([GitResult].self, from: data)
               // print(json)
                if let json = json {
                    comletionHandler(json)
                    print(json)
                } else {
                    print("can't decode")
                }
            }

        }.resume()
        
    }
    
    private static func imageData(url: URL,
                                completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    public static func downloadImage(url: URL,
                                     completion: @escaping (Result<Data>) -> Void) {
        imageData(url: url) { data, response, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data, error == nil else {
                return
            }
            
            DispatchQueue.main.async() {
                completion(.success(data))
            }
        }
    }
    
}
