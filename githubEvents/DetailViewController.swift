//
//  DetailViewController.swift
//  githubEvents
//
//  Created by Slava Starovoitov on 24.08.2021.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet var moduleView: UIView!
    @IBOutlet var avatarFull: UIImageView!
    @IBOutlet var accauntName: UILabel!
    @IBOutlet var repositoryName: UILabel!
    
    var avatarURL: String?
    var login: String?
    var repoName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "bg")
        self.navigationController!.navigationBar.topItem!.title = "To the list"
        
        //MARK: - Properties
        moduleView.moduleView()
        avatarFull.layer.cornerRadius = 15
        accauntName.mainText()
        repositoryName.semiText()
        
        //MARK: - Getting data
        avatarFull.load(urlString: avatarURL!)
        accauntName.text = login
        repositoryName.text = repoName
    }
    

}
