//
//  Extencions.swift
//  githubEvents
//
//  Created by Slava Starovoitov on 25.08.2021.
//

import UIKit

extension UILabel {
    
    func title() {
        self.font = UIFont.systemFont(ofSize: 34, weight: .bold)
        self.textColor = UIColor(named: "mainText")
        self.text = "Github Events 🔍"
    }
    
    func mainText() {
        self.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        self.textColor = UIColor(named: "mainText")
    }
    
    func semiText() {
        self.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        self.textColor = UIColor(named: "semiText")
    }
    
}

extension UIImageView {
    
    func load(urlString: String) {
        
        let imageCache = NSCache<AnyObject, AnyObject>()
        
        guard let url = URL(string: urlString) else { return }
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) {
            image = imageFromCache as? UIImage
            return
        }
        
        NetworkManager.downloadImage(url: url) { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    guard let imageToCache = UIImage(data: data) else { return }
                    imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                    self.image = UIImage(data: data)
                case .failure(_):
                    self.image = UIImage(named: "avatar")
                }
            }
        }
        
    }
    
}

extension UIView {
    func moduleView() {
        
        self.backgroundColor = UIColor(named: "main")
        self.layer.cornerRadius = 20
        self.layer.shadowColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowRadius = 25
        
    }
}

func convertDate(_ date: String) -> String {
    return date.replacingOccurrences(of: "-", with: "/", options: .regularExpression).filter{ $0 != "Z"  }.replacingOccurrences(of: "T", with: " ", options: .regularExpression)
}

