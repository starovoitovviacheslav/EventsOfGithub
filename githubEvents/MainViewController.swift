//
//  ViewController.swift
//  githubEvents
//
//  Created by Slava Starovoitov on 24.08.2021.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var data = [GitResult]()
    var currentPage: Int = 1
    var isLoadingList: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "bg")
        
        titleLabel.title()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        getDataFromServer(1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}

//MARK: - TableViewDelegate
extension MainViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let cellSpacingHeight: CGFloat = 10
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
        
        cell.nameLabel.mainText()
        cell.eventLabel.semiText()
        cell.dateLabel.semiText()
        
        cell.avatarImage.layer.cornerRadius = 3
        cell.avatarImage.layer.masksToBounds = true
        
        cell.backgroundColor = UIColor(named: "main")
        cell.layer.cornerRadius = 7
        cell.clipsToBounds = true
        
        cell.nameLabel.text = data[indexPath.section].actor?.login
        cell.eventLabel.text = data[indexPath.section].type
        cell.dateLabel.text = convertDate(data[indexPath.section].created_at ?? "no date")
        cell.avatarImage.load(urlString: (data[indexPath.section].actor?.avatar_url)!)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        
        detailVC.avatarURL = data[indexPath.section].actor?.avatar_url
        detailVC.login = data[indexPath.section].actor?.login
        detailVC.repoName = data[indexPath.section].repo?.name
        
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

//MARK: - GetMoreData Method
extension MainViewController {
    func getDataFromServer(_ pageNumber: Int) {
        NetworkManager.shared.getData(pageNum: pageNumber) { result in
            self.data.append(contentsOf: result)
            self.isLoadingList = false
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func loadMoreData() {
        currentPage += 1
        getDataFromServer(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height) && !isLoadingList) {
            self.isLoadingList = true
            self.loadMoreData()
        }
    }
}

