//
//  GitModel.swift
//  githubEvents
//
//  Created by Slava Starovoitov on 25.08.2021.
//

import Foundation

struct GitResult: Codable {
    let actor: Actor?
    let created_at: String?
    let repo: Repo?
    let type: String?
    
    static func empty() -> GitResult {
        return GitResult(actor: nil, created_at: nil, repo: nil, type: nil)
    }
    
}

struct Actor: Codable {
    let avatar_url: String?
    let login: String?
}

struct Repo: Codable {
    let name: String?
}
